package co.com.utest.questions;

import co.com.utest.model.DataUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

import java.util.List;

import static co.com.utest.userinterface.LastStep.LAST_STEP_LABEL;

public class Respuesta implements Question<Boolean> {
    private final List<DataUtest> lastStepLabel;

    public Respuesta(List<DataUtest> lastStepLabel) {
        this.lastStepLabel = lastStepLabel;
    }

    public static Respuesta onLastStep(List<DataUtest> lastStepLabel) {
        return new Respuesta(lastStepLabel);
    }

    @Override
    public Boolean answeredBy(Actor actor) {
        String labelConfirm = Text.of(LAST_STEP_LABEL).viewedBy(actor).asString();
        return lastStepLabel.get(0).getLastStepLabel().equals(labelConfirm);
    }
}
