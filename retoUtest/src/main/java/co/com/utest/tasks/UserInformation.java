package co.com.utest.tasks;

import co.com.utest.model.DataUtest;
import co.com.utest.model.DataUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;

import java.util.List;

import static co.com.utest.userinterface.UserInformationStep.*;

public class UserInformation implements Task {
    private final List<DataUtest> personalData;

    public UserInformation(List<DataUtest> personalData) {
        this.personalData = personalData;
    }

    public static UserInformation withPersonal(List<DataUtest> personalData) {
        return Tasks.instrumented(UserInformation.class, personalData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TXT_FIRST_NAME),
                Enter.theValue(personalData.get(0).getFirstName()).into(TXT_FIRST_NAME),
                Click.on(TXT_LAST_NAME),
                Enter.theValue(personalData.get(0).getLastName()).into(TXT_LAST_NAME),
                Click.on(TXT_EMAIL),
                Enter.theValue(personalData.get(0).getEmail()).into(TXT_EMAIL),
                Click.on(TXT_MONTH_BIRTH),
                SelectFromOptions.byVisibleText(personalData.get(0).getMonthBirth()).from(TXT_MONTH_BIRTH),
                Click.on(TXT_DAY_BIRTH),
                SelectFromOptions.byVisibleText(personalData.get(0).getDayBirth()).from(TXT_DAY_BIRTH),
                Click.on(TXT_YEAR_BIRTH),
                SelectFromOptions.byVisibleText(personalData.get(0).getYearBirth()).from(TXT_YEAR_BIRTH),
                Click.on(TXT_LANGUAGE_SPOKEN),
                Enter.theValue(personalData.get(0).getLanguageSpoken()).into(TXT_LANGUAGE_SPOKEN),
                Click.on(BTN_NEXT_STEP_LOCATION));
    }
}
