package co.com.utest.tasks;

import co.com.utest.model.DataUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.utest.userinterface.DevicesInformationStep.*;

public class DevicesInformation implements Task {
    private final List<DataUtest> deviceData;

    public DevicesInformation(List<DataUtest> deviceData) {
        this.deviceData = deviceData;
    }

    public static DevicesInformation withDevice(List<DataUtest> deviceData) {
        return Tasks.instrumented(DevicesInformation.class,deviceData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TXT_COMPUTER_OS),
                Enter.theValue(deviceData.get(0).getComputerOs()).into(TXT_COMPUTER_OS_INPUT),
                Hit.the(Keys.ENTER).into(TXT_COMPUTER_OS_INPUT),
                Click.on(TXT_COMPUTER_VERSION),
                Enter.theValue(deviceData.get(0).getComputerVersion()).into(TXT_COMPUTER_VERSION_INPUT),
                Hit.the(Keys.ENTER).into(TXT_COMPUTER_VERSION_INPUT),
                Click.on(TXT_COMPUTER_LANGUAGE),
                Enter.theValue(deviceData.get(0).getComputerLanguage()).into(TXT_COMPUTER_LANGUAGE_INPUT),
                Hit.the(Keys.ENTER).into(TXT_COMPUTER_LANGUAGE_INPUT),
                Click.on(TXT_MOBILE_BRAND),
                Enter.theValue(deviceData.get(0).getMobileBrand()).into(TXT_MOBILE_BRAND_INPUT),
                Hit.the(Keys.ENTER).into(TXT_MOBILE_BRAND_INPUT),
                Click.on(TXT_MOBILE_MODEL),
                Enter.theValue(deviceData.get(0).getMobileModel()).into(TXT_MOBILE_MODEL_INPUT),
                Hit.the(Keys.ENTER).into(TXT_MOBILE_MODEL_INPUT),
                Click.on(TXT_MOBILE_OS),
                Enter.theValue(deviceData.get(0).getMobileOs()).into(TXT_MOBILE_OS_INPUT),
                Hit.the(Keys.ENTER).into(TXT_MOBILE_OS_INPUT),
                Click.on(LAST_STEP));
    }
}
