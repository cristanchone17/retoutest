package co.com.utest.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;

import static co.com.utest.userinterface.HomeRegistrationStep.BTN_JOIN_REGISTRATION;

public class HomeRegistration implements Task {
    public static HomeRegistration withJoinButton() {
        return Tasks.instrumented(HomeRegistration.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(BTN_JOIN_REGISTRATION)
        );
    }
}
