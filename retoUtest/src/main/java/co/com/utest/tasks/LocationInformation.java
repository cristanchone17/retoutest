package co.com.utest.tasks;

import co.com.utest.model.DataUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.Hit;
import org.openqa.selenium.Keys;

import java.util.List;

import static co.com.utest.userinterface.LocationInformationStep.*;

public class LocationInformation implements Task {
    private final List<DataUtest> addressData;

    public LocationInformation(List<DataUtest> addressData) {
        this.addressData = addressData;
    }

    public static LocationInformation withAddress(List<DataUtest> addressData) {
        return Tasks.instrumented(LocationInformation.class,addressData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TXT_CITY),
                Enter.theValue(addressData.get(0).getCity()).into(TXT_CITY),
                Hit.the(Keys.ARROW_DOWN).into(TXT_CITY),
                Hit.the(Keys.ENTER).into(TXT_CITY),
                Click.on(TXT_POSTAL_CODE),
                Enter.theValue(addressData.get(0).getPostalCode()).into(TXT_POSTAL_CODE),
                Click.on(TXT_COUNTRY),
                Enter.theValue(addressData.get(0).getCountry()).into(TXT_COUNTRY_INPUT),
                Hit.the(Keys.ENTER).into(TXT_COUNTRY_INPUT),
                Click.on(BTN_NEXT_STEP_DEVICES));
    }
}
