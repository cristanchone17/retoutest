package co.com.utest.tasks;

import co.com.utest.userinterface.AbrirUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirBrowser implements Task {
    private AbrirUtest AbrirUtest;

    public static AbrirBrowser atUTestPage() {
        return Tasks.instrumented(AbrirBrowser.class);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Open.browserOn(AbrirUtest));
    }
}
