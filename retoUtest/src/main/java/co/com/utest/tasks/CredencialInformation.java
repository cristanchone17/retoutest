package co.com.utest.tasks;

import co.com.utest.model.DataUtest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

import java.util.List;

import static co.com.utest.userinterface.CredencialInformationStep.*;

public class CredencialInformation implements Task {
    private final List<DataUtest> privateData;

    public CredencialInformation(List<DataUtest> privateData) {
        this.privateData = privateData;
    }

    public static CredencialInformation withPrivate(List<DataUtest> privateData) {
        return Tasks.instrumented(CredencialInformation.class, privateData);
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(
                Click.on(TXT_PASSWORD),
                Enter.theValue(privateData.get(0).getPassword()).into(TXT_PASSWORD),
                Click.on(TXT_PASSWORD_CONFIRMATION),
                Enter.theValue(privateData.get(0).getPassword()).into(TXT_PASSWORD_CONFIRMATION),
                Click.on(CHECK_TERMS),
                Click.on(CHECK_POLICY));
    }
}
