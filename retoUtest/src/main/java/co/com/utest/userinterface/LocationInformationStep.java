package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class LocationInformationStep {
    public static final Target TXT_CITY = Target.the("write city location")
            .located(By.id("city"));
    public static final Target TXT_POSTAL_CODE = Target.the("write postal code location")
            .located(By.id("zip"));
    public static final Target TXT_COUNTRY = Target.the("select country location")
            .located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/div[1]/span"));
    public static final Target TXT_COUNTRY_INPUT = Target.the("write country location")
            .located(By.xpath("//*[@id='regs_container']/div/div[2]/div/div[2]/div/form/div[1]/div[3]/div[1]/div[4]/div[2]/div/div/input[1]"));

    public static final Target BTN_NEXT_STEP_DEVICES = Target.the("next step devices")
            .located(By.xpath("//a[@aria-label='Next step - select your devices']"));
}
