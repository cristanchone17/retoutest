package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class UserInformationStep {
    public static final Target TXT_FIRST_NAME = Target.the("write first name")
            .located(By.id("firstName"));
    public static final Target TXT_LAST_NAME = Target.the("write last name")
            .located(By.id("lastName"));
    public static final Target TXT_EMAIL = Target.the("write email")
            .located(By.id("email"));
    public static final Target TXT_MONTH_BIRTH = Target.the("select month birth")
            .located(By.id("birthMonth"));
    public static final Target TXT_DAY_BIRTH = Target.the("select day birth")
            .located(By.id("birthDay"));
    public static final Target TXT_YEAR_BIRTH = Target.the("select year birth")
            .located(By.id("birthYear"));

    public static final Target TXT_LANGUAGE_SPOKEN = Target.the("write language name")
            .located(By.xpath("//*[@id='languages']/div[1]/input"));
    public static final Target BTN_NEXT_STEP_LOCATION = Target.the("next step location")
            .located(By.xpath("//a[@aria-label='Next step - define your location']"));
}
