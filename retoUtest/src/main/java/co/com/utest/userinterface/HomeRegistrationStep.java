package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class HomeRegistrationStep {
    public static final Target BTN_JOIN_REGISTRATION = Target.the("start registration")
            .located(By.className("unauthenticated-nav-bar__sign-up"));
}
