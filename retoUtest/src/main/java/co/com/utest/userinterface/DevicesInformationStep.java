package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class DevicesInformationStep {
    public static final Target TXT_COMPUTER_OS = Target.the("select computer os field")
            .located(By.xpath("//*[@id='web-device']/div[1]/div[2]/div/div[1]/span"));
    public static final Target TXT_COMPUTER_OS_INPUT = Target.the("write computer os")
            .located(By.xpath("//*[@id='web-device']/div[1]/div[2]/div/input[1]"));
    public static final Target TXT_COMPUTER_VERSION = Target.the("select computer version field")
            .located(By.xpath("//*[@id='web-device']/div[2]/div[2]/div/div[1]/span"));
    public static final Target TXT_COMPUTER_VERSION_INPUT = Target.the("write computer version")
            .located(By.xpath("//*[@id='web-device']/div[2]/div[2]/div/input[1]"));
    public static final Target TXT_COMPUTER_LANGUAGE = Target.the("select computer language field")
            .located(By.xpath("//*[@id='web-device']/div[3]/div[2]/div/div[1]/span"));
    public static final Target TXT_COMPUTER_LANGUAGE_INPUT = Target.the("write computer language")
            .located(By.xpath("//*[@id='web-device']/div[3]/div[2]/div/input[1]"));
    public static final Target TXT_MOBILE_BRAND = Target.the("select mobile brand field")
            .located(By.xpath("//*[@id='mobile-device']/div[1]/div[2]/div/div[1]/span"));
    public static final Target TXT_MOBILE_BRAND_INPUT= Target.the("write mobile brand")
            .located(By.xpath("//*[@id='mobile-device']/div[1]/div[2]/div/input[1]"));
    public static final Target TXT_MOBILE_MODEL = Target.the("select mobile model field")
            .located(By.xpath("//*[@id='mobile-device']/div[2]/div[2]/div/div[1]/span"));
    public static final Target TXT_MOBILE_MODEL_INPUT = Target.the("write mobile model")
            .located(By.xpath("//*[@id='mobile-device']/div[2]/div[2]/div/input[1]"));
    public static final Target TXT_MOBILE_OS = Target.the("select mobile os field")
            .located(By.xpath("//*[@id='mobile-device']/div[3]/div[2]/div/div[1]/span"));
    public static final Target TXT_MOBILE_OS_INPUT = Target.the("write mobile os")
            .located(By.xpath("//*[@id='mobile-device']/div[3]/div[2]/div/input[1]"));

    public static final Target LAST_STEP = Target.the("last step")
            .located(By.xpath("//a[@aria-label='Next - final step']"));
}
