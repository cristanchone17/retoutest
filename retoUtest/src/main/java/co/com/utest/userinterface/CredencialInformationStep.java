package co.com.utest.userinterface;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class CredencialInformationStep {
    public static final Target TXT_PASSWORD = Target.the("write password")
            .located(By.id("password"));
    public static final Target TXT_PASSWORD_CONFIRMATION = Target.the("confirm passwrod")
            .located(By.id("confirmPassword"));
    public static final Target CHECK_TERMS = Target.the("accept terms")  // TERMINOS
            .located(By.id("termOfUse"));
    public static final Target CHECK_POLICY = Target.the("accept policy")  //POLITICAS
            .located(By.id("privacySetting"));
}
