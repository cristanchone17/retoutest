package co.com.utest.stepdefinitions;

import co.com.utest.model.DataUtest;
import co.com.utest.questions.Respuesta;
import co.com.utest.tasks.*;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.actors.OnlineCast;

import java.util.List;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class UTestStepDefinition {

    @Before
    public void setStage(){
        setTheStage(new OnlineCast());
    }

    @Given("^as Pedro want to create a new account at utest page$")
    public void asPedroWantToCreateANewAccountAtUtestPage() {
        theActorCalled("Pedro").wasAbleTo(AbrirBrowser.atUTestPage());
    }

    @When("^complete registration form$")
    public void completeRegistrationForm(List<DataUtest> data) {
        theActorInTheSpotlight().attemptsTo(
                HomeRegistration.withJoinButton(),
                UserInformation.withPersonal(data),
                LocationInformation.withAddress(data),
                DevicesInformation.withDevice(data),
                CredencialInformation.withPrivate(data)
        );
    }

    @Then("^validate label on last step of form$")
    public void validateLabelOnLastStepOfForm(List<DataUtest> label) {
        theActorInTheSpotlight().should(seeThat(Respuesta.onLastStep(label)));
    }
}
