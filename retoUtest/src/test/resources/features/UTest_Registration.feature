# Creador: Cristian Alfonso Meza Ortega

@stories
Feature: uTest Page
  As a user, want to create new account at utest page with registration form

  @scenario1
  Scenario Outline: Registration form
    Given as Pedro want to create a new account at utest page
    When complete registration form
      | firstName      | lastName      | email      | monthBirth      | dayBirth      | yearBirth      | languageSpoken      | city      | postalCode      | country      | computerOs      | computerVersion      | computerLanguage      | mobileBrand      | mobileModel      | mobileOs      | password      |
      | <strfirstName> | <strlastName> | <stremail> | <strmonthBirth> | <strdayBirth> | <stryearBirth> | <strlanguageSpoken> | <strcity> | <strpostalCode> | <strcountry> | <strcomputerOs> | <strcomputerVersion> | <strcomputerLanguage> | <strmobileBrand> | <strmobileModel> | <strmobileOs> | <strpassword> |
    Then validate label on last step of form
      | lastStepLabel      |
      | <strlastStepLabel> |

    Examples:
      | strfirstName | strlastName | stremail            | strmonthBirth | strdayBirth | stryearBirth | strlanguageSpoken | strcity  | strpostalCode | strcountry | strcomputerOs | strcomputerVersion | strcomputerLanguage | strmobileBrand | strmobileModel | strmobileOs | strpassword   | strlastStepLabel |
      | Cristian | Meza       | cristianmeza@yopmail.com | September          | 13           | 1998         | Spanish           | Envigado | 542687        | Colombia  | Windows         | Vista             | English             | Apple         | iPhone          | iOS 2.0          | Utest2022*. | The last step    |